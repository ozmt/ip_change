# Setup the management network

mg10g='TRUE'

my_ip=`cat zfs_ip_list|grep "^${HOSTNAME}" | awk -F " " '{print $2}'`

echo "My ip: $my_ip"

source /etc/ozmt/config.${HOSTNAME}

dgw=`netstat -rnv |grep default|awk -F " " '{print $3}'`

mgip="$my_ip"
mgsn='24'
mggw='10.27.115.254'
# Pool IP interfaces.  Select the right interface
pif1="$wucon_nic1"
pif2="$wucon_nic1"


    # Setup vlan 2308 and the management IP
    dladm create-vlan -l \$mgif1 -v 2308 vlan2308v0
    dladm create-vlan -l \$mgif2 -v 2308 vlan2308v1
    ifconfig vlan2308v0 plumb up
    ifconfig vlan2308v1 plumb up
    ifconfig vlan2308i0 ipmp \$mgip/\$mgsn up
    echo "ipmp group vlan2308i0 \$mgip/\$mgsn up" > /etc/hostname.vlan2308i0
    ifconfig vlan2308v0 -failover group vlan2308i0 up
    ifconfig vlan2308v1 -failover group vlan2308i0 up
    echo "group vlan2308i0 -failover up" > /etc/hostname.vlan2308v0
    echo "group vlan2308i0 -failover up" > /etc/hostname.vlan2308v1
    # Now set primary mgmt IP on \$mgif1
    # ipadm create-addr -T static -a \$mgip/\$mgsn vlan2308i0/v4

# Now the default route
route -p add default \$mggw
# Populate /etc/hosts, adjust to match your hostname.domain
cat /etc/hosts|grep -v ${HOSTNAME} > /tmp/hosts
cp /tmp/hosts /etc/hosts
echo "\$mgip \${HOSTNAME}.zfs.mir \${HOSTNAME}" >> /etc/hosts
# DNS Config, adjusting domain and search as needed
echo 'nameserver 10.27.112.65' > /etc/resolv.conf
echo 'nameserver 10.27.112.66' >> /etc/resolv.conf
echo 'search zfs.mir neuroimage.wustl.edu nrg.wustl.edu nrg.mir' >> /etc/resolv.conf

echo "Press any key to remove the old default route: $dgw"
echo "net delete 0.0.0.0 gw $dgw"
